# BackOffice

LibShare back-end repository

## Development guide

_Work in progress_

```bash
docker-compose -f dev.docker-compose.yml up --force-recreate --build
```

### Curl request

Set the https URL:

```bash
URL=https://my.url
```

Base command:

```bash
curl -X GET -k -i "${URL}/path"
curl -X POST -k -H 'Content-Type: application/json' -i "${URL}/path" --data '{
    "key": "value",
    "key": "value",
    "key": "value"
}'
```

#### Example:

Authenticate, get token, get document list

```bash
curl -X POST -k -H 'Content-Type: application/json' "${URL}/register" --data '{
    "email": "test@test.test",
    "password": "test",
    "username": "test"
}'
json=$(curl -X POST -k -H 'Content-Type: application/json' "${URL}/authenticate" --data '{
    "email": "test@test.test",
    "password": "test"
}')
token=$( jq -r ".token" <<<"$json" )
curl -X GET -k -H 'Content-Type: application/json' -H "Authorization: Bearer ${token}" "${URL}/documents"
```

## Run pytest

- Build the image:

```bash
docker compose -f ./tests.docker-compose.yml up --force-recreate --build -d
```

- Launch docker and enter it:

```bash
docker exec -it libshare_backend_test sh
```

- Run linter:

```bash
pylint libshare/libshare
pylint-json2html -o pylint.html pylint.json
```

- Run tester:

```bash
pytest --cov=libshare/ --cov-fail-under=80 --cov-report=html:./coverage --html=test_report.html --self-contained-html --verbose -r A --full-trace
```
