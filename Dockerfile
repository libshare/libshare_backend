FROM python:alpine

# Set the working directory to /app
WORKDIR /libshare

# Copy the current directory contents into the container at /app
COPY ./libshare /libshare

RUN apk add postgresql-dev gcc python3-dev musl-dev libffi-dev openssl-dev cargo

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 8080

# Run the app.
CMD gunicorn --bind 0.0.0.0:8080 "libshare:create_app('')"

