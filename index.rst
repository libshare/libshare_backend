.. Libshare documentation master file, created by
   sphinx-quickstart on Sun Aug 13 14:08:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Libshare's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./README_old
   ./README
   ./docstring/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

