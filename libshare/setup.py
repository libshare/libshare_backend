from setuptools import setup

setup(
    name='libshare',
    packages=['libshare'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
