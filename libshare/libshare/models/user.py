"""User model module"""
from dataclasses import dataclass


@dataclass
class User():
    """User model

    Attributes
    ----------
    userid : int, optional
        user unique ID, by default None
    username : str
        user name
    userpassword : str
        user password
    email : str
        user email address
    """

    username: str
    userpassword: str
    email: str
    id: int = 0  # pylint: disable=invalid-name

    @classmethod
    def init_from_dict(cls, data: dict):
        """Init user information from unordered data

        :param data: user data information
        :type data: dict
        :return: user created from data
        :rtype: _type_
        """
        new_user = cls(data['username'], data['userpassword'], data['email'])
        new_user.__dict__.update(data)
        return new_user
