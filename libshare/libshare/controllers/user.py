"""User management API"""
from flask import request, jsonify, Blueprint
from flask_cors import cross_origin
from flask_jwt_extended import create_access_token
from werkzeug.security import check_password_hash, generate_password_hash

import libshare.services.user as service

userAPI = Blueprint('user', __name__, url_prefix='')


@userAPI.route('/authenticate', methods=['POST'])
@cross_origin()
def login():
    """User login function.

    Returns
    -------
    response
        json with token or explicit error message.

    Notes
    -----
    **Route**
        - /authenticate
    **Method**
        - POST
    **Request**
        - JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "email": "string",
                    "password": "string"
                }

    **Response**
        - 200, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "token": "JWT access token"
                }

        - 400, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "msg": "error message"
                }

    """
    user = service.get(request.json.get('email', None))
    if check_password_hash(user.userpassword, request.json.get('password', None)):
        access_token = create_access_token(identity=user.email)
        return jsonify(token=access_token), 200

    return jsonify({'msg': 'Wrong email/password'}), 400


@userAPI.route('/register', methods=['POST'])
@cross_origin()
def register():
    """Register a user.

    Returns
    -------
    response
        200 with token or error message

    Notes
    -----
    **Route**
        - /register
    **Method**
        - POST
    **Request**
        - JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "email": "string",
                    "username": "string",
                    "password": "string"
                }

    **Response**
        - 200, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "token": "JWT access token"
                }

        - 400, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "msg": "error message"
                }

    """
    data = request.json
    user = service.add({
        'username': data['username'],
        'userpassword': generate_password_hash(data['password']),
        'email': data['email']
    })
    return jsonify(token=create_access_token(identity=user.email)), 200
