"""Libshare API controller"""

from flask import Blueprint
from flask_cors import cross_origin

main = Blueprint('main', __name__, url_prefix='')


@main.route('/')
@cross_origin()
def hello() -> str:
    """Get API welcome message.

    This URL helps you check your server is running.

    Returns
    -------
    str
        API welcome message

    Notes
    -----
    **Route**
        - /
    **Method**
        - GET
    **Response**
        - 200, welcome message
        - 500, internal error
    """
    return 'Welcome to LibShare API!'
