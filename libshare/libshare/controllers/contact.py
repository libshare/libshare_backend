"""Contact API"""

from flask import request, jsonify, Blueprint
from flask_cors import cross_origin
from flask_jwt_extended import get_jwt_identity, jwt_required

import libshare.services.contact as service

contact = Blueprint('contact', __name__, url_prefix='')


@contact.route('/contact/<contact_id>', methods=['GET'])
@jwt_required()
@cross_origin()
def get_contact(contact_id: int) -> dict:
    """Get contact info based on its id

    :param contact_id: id of contact to retrieve
    :type contact_id: int
    :return: found contact
    :rtype: dict
    """
    return jsonify(service.get(get_jwt_identity(), contact_id))


@contact.route('/addContact', methods=['PUT'])
@jwt_required()
@cross_origin()
def add_contact() -> dict:
    """Add a contact

    :return: added contact
    :rtype: dict
    """
    return jsonify(service.add(get_jwt_identity(), request.json))


@contact.route('/contact', methods=['GET'])
@jwt_required()
@cross_origin()
def get_all_or_queried() -> list[dict]:
    """Get all or queried contacts

    :return: a list of contact
    :rtype: list[dict]
    """
    identity = get_jwt_identity()
    query = request.args
    if query:
        return jsonify(service.get_by_query(identity, query.get('search', None)))
    return jsonify(service.get_all(identity))


@contact.route('/updateContact', methods=['POST'])
@jwt_required()
@cross_origin()
def update() -> dict:
    """Update contact information

    :return: updated contact
    :rtype: dict
    """
    return jsonify(service.update(get_jwt_identity(), request.json))


@contact.route('/deleteContact/<contact_id>', methods=['DELETE'])
@jwt_required()
@cross_origin()
def delete(contact_id: int) -> dict:
    """Delete contact based on its id

    :param contact_id: id of contact to delete
    :type contact_id: int
    :return: deleted contact information
    :rtype: dict
    """
    return jsonify(service.delete(get_jwt_identity(), contact_id))
