"""User database queries"""

#: Add a user
ADD = ("INSERT INTO public.users "
            "(username, email, userpassword) "
            "VALUES(%(username)s, %(email)s, %(userpassword)s) "
            "RETURNING *;")

#: Get a user by its email address
GET_BY_EMAIL = ("SELECT id, username, email, userpassword "
                     "FROM public.users "
                     "WHERE email = %(email)s;")
