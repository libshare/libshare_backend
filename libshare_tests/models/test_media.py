from libshare.models.media import Media
import unittest


class TestMedia(unittest.TestCase):

    def test_init_from_dict__base_ok(self):
        data = {'userid': 42, 'isbn': 'Wayne',
                'title': 'owl', 'authors': 'Robert'}
        result = Media.init_from_dict(data)
        self.assertEqual(data['userid'], result.userid)
        self.assertEqual(data['isbn'], result.isbn)
        self.assertEqual(data['title'], result.title)
        self.assertEqual(data['authors'], result.authors)
        self.assertEqual(0, result.id)
        self.assertEqual('', result.mediaabstract)
        self.assertEqual(1, result.numberofmedia)
        self.assertEqual(None, result.shared_id)

    def test_init_from_dict__missing_key_error(self):
        data = {'userid': 42, 'isbn': 1}
        with self.assertRaises(KeyError):
            myMedia = Media.init_from_dict(data)

    def test_init_from_dict__add_other(self):
        data = {
            'userid': 42,
            'isbn': 'Wayne',
            'title': 'owl',
            'authors': 'Robert',
            'id': 42
        }
        result = Media.init_from_dict(data)
        self.assertEqual(data['userid'], result.userid)
        self.assertEqual(data['isbn'], result.isbn)
        self.assertEqual(data['title'], result.title)
        self.assertEqual(data['authors'], result.authors)
        self.assertEqual(data['id'], result.id)
        self.assertEqual('', result.mediaabstract)
        self.assertEqual(1, result.numberofmedia)
        self.assertEqual(None, result.shared_id)

    def test_init_from_dict__add_unknown(self):
        data = {
            'userid': 42,
            'isbn': 'Wayne',
            'title': 'owl',
            'authors': 'Robert',
            'id': 42,
            'butler': 'Alfred'
        }
        result = Media.init_from_dict(data)
        self.assertEqual(data['userid'], result.userid)
        self.assertEqual(data['isbn'], result.isbn)
        self.assertEqual(data['title'], result.title)
        self.assertEqual(data['authors'], result.authors)
        self.assertEqual(data['id'], result.id)
        self.assertEqual('', result.mediaabstract)
        self.assertEqual(1, result.numberofmedia)
        self.assertEqual(None, result.shared_id)
        self.assertEqual(data['butler'], result.butler)
