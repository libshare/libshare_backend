from libshare.models.contact import Contact
import unittest


class TestContact(unittest.TestCase):

    def test_init_from_dict__base_ok(self):
        data = {'first_name': 'Bruce', 'last_name': 'Wayne', 'user_id': 1}
        myContact = Contact.init_from_dict(data)
        self.assertEqual(data['first_name'], myContact.first_name)
        self.assertEqual(data['last_name'], myContact.last_name)
        self.assertEqual(data['user_id'], myContact.user_id)
        self.assertEqual(0, myContact.id)
        self.assertEqual('', myContact.email)
        self.assertEqual('', myContact.phone)
        self.assertEqual('', myContact.information)

    def test_init_from_dict__missing_key_error(self):
        data = {'first_name': 'Bruce', 'user_id': 1}
        with self.assertRaises(KeyError):
            myContact = Contact.init_from_dict(data)

    def test_init_from_dict__add_other(self):
        data = {'first_name': 'Bruce', 'last_name': 'Wayne', 'user_id': 'titi', 'id': 4,
                'email': 'bruce.wayne@bat.inc', 'phone': '555', 'information': 'Batman'}
        myContact = Contact.init_from_dict(data)
        self.assertEqual(data['first_name'], myContact.first_name)
        self.assertEqual(data['last_name'], myContact.last_name)
        self.assertEqual(data['user_id'], myContact.user_id)
        self.assertEqual(data['id'], myContact.id)
        self.assertEqual(data['email'], myContact.email)
        self.assertEqual(data['phone'], myContact.phone)
        self.assertEqual(data['information'], myContact.information)

    def test_init_from_dict__add_unknown(self):
        data = {'first_name': 'Bruce', 'last_name': 'Wayne', 'user_id': 'titi', 'id': 4,
                'email': 'bruce.wayne@bat.inc', 'phone': '555', 'information': 'Batman', 'butler': 'penny one'}
        myContact = Contact.init_from_dict(data)
        self.assertEqual(data['first_name'], myContact.first_name)
        self.assertEqual(data['last_name'], myContact.last_name)
        self.assertEqual(data['user_id'], myContact.user_id)
        self.assertEqual(data['id'], myContact.id)
        self.assertEqual(data['email'], myContact.email)
        self.assertEqual(data['phone'], myContact.phone)
        self.assertEqual(data['information'], myContact.information)
        self.assertEqual(data['butler'], myContact.butler)
