from libshare.models.user import User
import unittest


class TestUser(unittest.TestCase):

    def test_init_from_dict__base_ok(self):
        data = {'username': 'Wayne', 'userpassword': 'robin', 'email': 'joker'}
        result = User.init_from_dict(data)
        self.assertEqual(data['username'], result.username)
        self.assertEqual(data['userpassword'], result.userpassword)
        self.assertEqual(data['email'], result.email)
        self.assertEqual(0, result.id)

    def test_init_from_dict__missing_key_error(self):
        data = {'username': 'Wayne', 'userpassword': 'robin'}
        with self.assertRaises(KeyError):
            myUser = User.init_from_dict(data)

    def test_init_from_dict__add_other(self):
        data = {
            'username': 'Wayne',
            'userpassword': 'robin',
            'email': 'joker',
            'id': 42
            }
        result = User.init_from_dict(data)
        self.assertEqual(data['username'], result.username)
        self.assertEqual(data['userpassword'], result.userpassword)
        self.assertEqual(data['email'], result.email)
        self.assertEqual(data['id'], result.id)

    def test_init_from_dict__add_unknown(self):
        data = {
            'username': 'Wayne',
            'userpassword': 'robin',
            'email': 'joker',
            'id': 42,
            'butler': 'Alfred'
            }
        result = User.init_from_dict(data)
        self.assertEqual(data['username'], result.username)
        self.assertEqual(data['userpassword'], result.userpassword)
        self.assertEqual(data['email'], result.email)
        self.assertEqual(data['id'], result.id)
        self.assertEqual(data['butler'], result.butler)
