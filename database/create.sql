CREATE TABLE public.users (
    id SERIAL PRIMARY KEY,
    username VARCHAR (250) NOT NULL UNIQUE,
    email VARCHAR(250) NOT NULL UNIQUE,
    userpassword VARCHAR (250) NOT NULL
);
CREATE TABLE public.contact (
    id SERIAL PRIMARY KEY,
    first_name TEXT,
    last_name TEXT,
    email TEXT,
    phone TEXT,
    information TEXT,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);
CREATE TABLE public.media (
    id SERIAL PRIMARY KEY,
    isbn VARCHAR (250),
    title VARCHAR (250) NOT NULL,
    authors VARCHAR (250),
    mediaabstract TEXT,
    cover TEXT,
    numberofmedia INT,
    userid INTEGER NOT NULL,
    shared_id INTEGER,
    FOREIGN KEY (userid) REFERENCES users(id),
    FOREIGN KEY (shared_id) REFERENCES contact(id)
);
